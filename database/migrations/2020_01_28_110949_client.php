<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Client extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		//
		Schema::create('client', function (Blueprint $table) {
				$table->increments('id');
				$table->unsignedInteger('user_id');
				$table->bigInteger('hostname_id')->unsigned()->nullable();
				$table->string('name');
				$table->string('subdomain')->nullable();
				$table->timestamps();

				$table->foreign('user_id')->references('id')->on('users');
				$table->foreign('hostname_id')->references('id')->on('hostnames');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
		Schema::dropIfExists('client');
	}
}
