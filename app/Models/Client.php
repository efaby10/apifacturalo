<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Client extends Model {

	protected $table    = 'client';
	protected $with     = ['user'];
	protected $fillable = [
		'name',
		'subdomain',
		'user_id',
		'hostname_id'
	];

	public function user() {
		return $this->belongsTo(User::class );
	}
}