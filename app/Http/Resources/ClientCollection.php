<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientCollection extends ResourceCollection {
	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return mixed
	 */
	public function toArray($request) {
		return $this->collection->transform(function ($row, $key) {
				return [
					'id'        => $row->id,
					'subdomain' => $row->subdomain,
					'name'      => $row->name,
					'date'      => $row->created_at,
					'email'     => $row->user->email,
				];
			});
	}
}