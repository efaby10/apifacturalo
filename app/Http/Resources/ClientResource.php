<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'id'        => $this->id,
			'email'     => $this->user->email,
			'name'      => $this->name,
			'subdomain' => $this->subdomain,
		];
	}
}