<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Http\Resources\ClientCollection;
use App\Http\Resources\ClientResource;

use App\Models\Client;
use App\Models\User;

use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;

use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Models\Hostname;

use Hyn\Tenancy\Models\Website;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

class ClientController extends Controller {

	public $domain = "demo.com";

	public function index() {
		return view('clients.index');
	}

	public function columns() {
		return [
			'id'   => 'Código',
			'name' => 'Nombre'
		];
	}

	public function records(Request $request) {
		$records = Client::where($request->column, 'like', "%{$request->value}%")
			->orderBy('name');

		return new ClientCollection($records->paginate(env('ITEMS_PER_PAGE', 5)));
	}

	public function record($id) {
		$record = new ClientResource(Client::findOrFail($id));

		return $record;
	}

	public function store(ClientRequest $request) {
		// create user
		$id          = $request->input('id');
		$user        = User::firstOrNew(['id' => $id]);
		$user->name  = $request->input('name');
		$user->email = $request->input('email');
		if (!$id) {
			$user->api_token = Str::random(50);
			$user->password  = bcrypt($request->input('password'));
		} elseif ($request->input('password') !== '') {
			$user->password = bcrypt($request->input('password'));
		}
		$user->save();

		// create website and create table and user in database
		$subdomain                               = $request->input('subdomain').".".$this->domain;
		$website                                 = new Website;
		$website->uuid                           = Str::random(10);
		$website->managed_by_database_connection = 'system';
		app(WebsiteRepository::class )->create($website);
		$hostname       = new Hostname;
		$hostname->fqdn = $subdomain;
		$hostname       = app(HostnameRepository::class )->create($hostname);
		app(HostnameRepository::class )->attach($hostname, $website);

		// create client
		$id                  = $request->input('id');
		$client              = Client::firstOrNew(['id' => $id]);
		$client->name        = $request->input('name');
		$client->subdomain   = $subdomain;
		$client->user_id     = $user->id;
		$client->hostname_id = $hostname->id;
		$client->save();

		return [
			'success' => true,
			'message' => ($id)?'Cliente actualizado':'Cliente registrado'
		];
	}

	public function destroy($id) {
		$client = Client::findOrFail($id);
		;
		$user = User::findOrFail($client->user_id);
		$client->delete();
		$user->delete();

		return [
			'success' => true,
			'message' => 'Cliente eliminado con éxito'
		];
	}
}